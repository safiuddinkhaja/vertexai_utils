from typing import List
from langchain.embeddings import VertexAIEmbeddings
import numpy as np


class VertexAIEmbeddingsX(VertexAIEmbeddings):

    def embed_query(self, text: str) -> np.ndarray:
        return np.array(super().embed_query(text))

    def embed_documents(self, texts: List[str], batch_size: int = 5) -> np.ndarray:
        embeddings = super().embed_documents(texts, batch_size)
        return np.vstack(embeddings)