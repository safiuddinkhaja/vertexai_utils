"""Basic utility functions"""
from functools import wraps
import time

from google.api_core.exceptions import ResourceExhausted

def exponential_backoff(max_tries=5, initial_delay=1, max_delay=60):
    """
    A decorator that retries a function with exponential backoff.

    Args:
        max_tries: The maximum number of times to retry the function.
        initial_delay: The initial delay in seconds before the first retry.
        max_delay: The maximum delay in seconds before any retry.

    Returns:
        A decorator that retries the decorated function with exponential backoff.
    """

    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            for attempt in range(1, max_tries + 1):
                try:
                    return func(*args, **kwargs)
                except ResourceExhausted:
                    delay = min(2 ** (attempt - 1) * initial_delay, max_delay)
                    print(f"Retrying in {delay} seconds...")
                    time.sleep(delay)

            return func(*args, **kwargs)

        return wrapper

    return decorator

def exponential_backoff_async(max_tries=5, initial_delay=1, max_delay=60):
    """
    A decorator that retries an async function with exponential backoff.

    Args:
        max_tries: The maximum number of times to retry the function.
        initial_delay: The initial delay in seconds before the first retry.
        max_delay: The maximum delay in seconds before any retry.

    Returns:
        A decorator that retries the decorated async function with exponential backoff.
    """

    def decorator(func):
        @wraps(func)
        async def wrapper(*args, **kwargs):
            for attempt in range(1, max_tries + 1):
                try:
                    response = await func(*args, **kwargs)
                    return response
                except ResourceExhausted:
                    delay = min(2 ** (attempt - 1) * initial_delay, max_delay)
                    print(f"Retrying in {delay} seconds...")
                    time.sleep(delay)

            response = await func(*args, **kwargs)
            return response

        return wrapper

    return decorator