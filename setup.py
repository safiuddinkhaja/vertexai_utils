# Write blank setup.py
import pkg_resources
from setuptools import find_packages, setup

setup(
    name="vertexai_utils",
    version="0.0.1",
    description="Basic helper functions for Vertex AI LLM API",
    author="Safiuddin Khaja",
    author_email="safiuddinkhaja@google.com",
    packages=find_packages(),
    install_requires=[
        str(r)
        for r in pkg_resources.parse_requirements(
            open("requirements.txt")
        )]
)