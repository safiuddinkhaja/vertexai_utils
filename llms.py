"""Module for LLM classes"""
from typing import Optional, List, Any
import logging

from langchain.llms import VertexAI

from vertexai_utils.utils import exponential_backoff, exponential_backoff_async

class VertexAIX(VertexAI):
    """Vertex AI with Extended functionality like exponential backoff and logging"""
    @exponential_backoff(max_tries=10)
    def __call__(self,
                 prompt: str,
                 stop: Optional[List[str]] = None,
                 logger: Optional[logging.Logger] = None,
                 **kwargs: Any
                 ) -> str:
        if logger:
            logger.info(f"prompt: {prompt}")
        response = self._generate([prompt], stop, **kwargs).generations[0][0].text
        if logger:
            logger.info(f"response: {response}")
        return response

    @exponential_backoff_async(max_tries=10)
    async def apredict(self,
                 text: str,
                 stop: Optional[List[str]] = None,
                 logger: Optional[logging.Logger] = None,
                 **kwargs: Any) -> str:
        if logger:
            logger.info(f"prompt: {text}")
        response = await self._agenerate([text], stop, **kwargs)
        response = response.generations[0][0].text
        if logger:
            logger.info(f"response: {response}")
        return response